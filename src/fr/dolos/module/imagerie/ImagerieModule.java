/*
 * Dolos Main Module class for Imagerie module 
 */
package fr.dolos.module.imagerie;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import fr.dolos.module.imagerie.network.packets.ControlImageryPacket;
import fr.dolos.module.imagerie.network.packets.FramePacket;
import fr.dolos.module.imagerie.network.packets.ImageryInfosPacket;
import fr.dolos.module.imagerie.network.packets.LabelSelectionPacket;
import fr.dolos.module.imagerie.network.packets.LabeledFramePacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveControlPacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveServerInfoPacket;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.modules.UserModule;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.NetworkManager;
import fr.dolos.sdk.network.PacketHandler;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;



/**
 *
 * @author schindler
 */
public class ImagerieModule /*extends UserModule*/ {
    
	/*private String name = "Module Imagerie test";
    private Core core = null;
    private NetworkManager networkManager;
    private int imgCount = 0;
    
    private NetworkClient client = null;
    private boolean viewLive = false;
    private boolean imageryStarted = false;
    private boolean opened = false;
    private boolean opening = false;
    private boolean liveActive = false;
	private FXMLLoader fxmlLoader = new FXMLLoader();
    
	//For connected drones
    @FXML
    private Label droneLabel;
	@FXML
	private ChoiceBox<Drone> droneSelect;
	
    // For video feed view
    @FXML
    private ImageView currentFrame;
    @FXML
    private AnchorPane videoPane;
    private Mat lastFrameReceived;	
    
    // For the label selection list
    @FXML
    private TableView<LabelSelect> labelTable;
    @FXML
    private TableColumn<LabelSelect, String> labelColumn;
    @FXML
    private TableColumn<LabelSelect, CheckBox> selectColumn;
    private final ObservableList<LabelSelect> availableLabels = FXCollections.observableArrayList();
    
    // For frames sent by neural network
    @FXML
    private ListView<Image> frameListView;
    private final ObservableList<Image> frameList = FXCollections.observableArrayList();
    
    
    @Override
    public boolean load(Core core) {
        this.core = core;
             
        // register the module's packet deserializers
        nu.pattern.OpenCV.loadShared();
        networkManager = core.getNetworkManager();
        networkManager.registerReceiver(this);
        networkManager.registerDeserializer("Imagery_Infos_Packet", new ImageryInfosPacket());
        networkManager.registerDeserializer("View_Live_Server_Info_Packet", new ViewLiveServerInfoPacket());
        networkManager.registerDeserializer("Labeled_Frame_Packet", new LabeledFramePacket());
        networkManager.registerDeserializer("View_Live_Control_Packet", new ViewLiveControlPacket());
        networkManager.registerDeserializer("Control_Imagery_Packet", new ControlImageryPacket());
       
        return true;
    }
    
   
    @Override
    public void unload() {
    	log("Unloading module");
    }
    
    @Override
    public String getName()
    {
       return name;
    }
    
    @Override
    public void update(){
    	if (!opened && !opening) {
			opening = true;
			createTabContent();
    	}
    	if (viewLive && this.lastFrameReceived != null) {
    		Platform.runLater(()->{
    			currentFrame.setImage(mat2Image(this.lastFrameReceived));
    		});  
	    }
    }
    
    /**
     * Recupere la reponse du drone après envoie de vViewLiveControlPacket
     * @param viewLiveControlPacket
     * @param client
     *
    @PacketHandler
    public void onPacketReceived(ViewLiveControlPacket viewLiveControlPacket, NetworkClient client) {
    	this.viewLive = viewLiveControlPacket.enable;
    	log("View Live : " + viewLiveControlPacket.enable);
    }
    
    /**
     * Recupere la reponse du drone après envoie de controlImageryPacket
     * @param controlImageryPacket
     * @param client
     *
    @PacketHandler
    public void onPacketReceived(ControlImageryPacket controlImageryPacket, NetworkClient client) {
    	this.imageryStarted = controlImageryPacket.enable;
    	log("Imagery : " + controlImageryPacket.enable);
    }

    /**
     *  Recupere le port de connection sur le drone pour le live video
     * @param serverInfoPacket
     * @param client
     *
    @PacketHandler
    public void onPacketReceived(ViewLiveServerInfoPacket serverInfoPacket, NetworkClient client) {

        int port = serverInfoPacket.getPort();
        ImagerieModule module = this;
        
        if (module.liveActive)
        	return;
       
        log("OPEN UDP");
        
        core.getScheduler().runOnDedicated(new Callable<Void>() {
            
            private String buffer = "";
            private DatagramSocket socket;
           
            @Override
            public Void call() {
                System.out.println("Runnable running");
                try {
                    String text = "ClientlLiveUDP";
                    
                    DatagramPacket data = new DatagramPacket(text.getBytes(), text.length(), InetAddress.getByName(client.getIp()), port);
                    socket = new DatagramSocket();
                    socket.send(data);
                    
                    while (module.viewLive) {
                        Mat frameReceived = readSocket();
                        if (frameReceived == null) {
                            continue;
                        }
                        lastFrameReceived = frameReceived;
                    }
                    module.liveActive = false;
                } catch (UnknownHostException ex) {
                	log(ex.getMessage());
                    Logger.getLogger(ImagerieModule.class.getName()).log(Level.SEVERE, null, ex);   
                } catch (SocketException ex) {
                	log(ex.getMessage());
                    Logger.getLogger(ImagerieModule.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                	log(ex.getMessage());
                    Logger.getLogger(ImagerieModule.class.getName()).log(Level.SEVERE, null, ex);
                }
				return null;
            }
            
            /**
             *  Parse les données envoyé par le serveur en image a reintegré coté application
             * @return
             *
            private Mat readSocket() {
                String packet_begin_str = "<Dolos_STR>";
                String packet_end_str = "</Dolos_STR>";
                byte[] buff = new byte[4096];
                
                while (true) {

                    try {
                        if (buffer.contains(packet_begin_str) && buffer.contains(packet_end_str)) {
                            break;
                        }
                        
                        DatagramPacket dataReceived = new DatagramPacket(buff, 4096);
                        socket.receive(dataReceived);
                        
                        byte[] packetReceived = dataReceived.getData();
                        String stringReceived = new String(packetReceived);
                        buffer += stringReceived;
                    } catch (IOException ex) {
                    	log(ex.getMessage());
                        Logger.getLogger(ImagerieModule.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

                int packet_start = buffer.indexOf(packet_begin_str) + packet_begin_str.length();
                int packet_end = buffer.indexOf(packet_end_str);

                byte[] packet;
                packet = Arrays.copyOfRange(buffer.getBytes(), packet_start, packet_end);
                buffer = buffer.substring(packet_end + packet_end_str.length(), buffer.length());

                Mat encoded = new Mat(1, packet.length, CvType.CV_8U);
                encoded.put(0, 0, packet);
                return Imgcodecs.imdecode(encoded, Imgcodecs.IMREAD_COLOR);
            }
        });
    }
    
    /**
     * recupere les frames pour lequel un label as été detecté 
     * @param framePacket
     * @param client
     *
    @PacketHandler
    public void onPacketReceived(LabeledFramePacket framePacket, NetworkClient client) {        
         log("Packet received : " + framePacket.getName());
         log("Packet label : " + framePacket.getLabel());
         imgCount++;
         Mat frame = framePacket.getFrame();
         Imgcodecs.imwrite("data_received/image_received_"+ imgCount+ ".jpg", frame);
         
         Image img  = mat2Image(frame);       
         this.frameList.add(img);

     } 
     
    
    /**
     * Retourne les labels disponible
     * @param infoPacket
     * @param client
     *
     @PacketHandler
     public void onPacketReceived(ImageryInfosPacket infoPacket, NetworkClient client) {
         this.imageryStarted = infoPacket.getStarted();
         this.availableLabels.clear();
         infoPacket.getLabels().forEach((label) -> {
             this.availableLabels.add(new LabelSelect(label));
         });
         this.client = client;
         this.droneLabel.setText(client.getIp());

         log("Available labels from drone : " + this.availableLabels.toString());
     }
    
    private void createTabContent() {
    	Platform.runLater(()-> {
        	Tab tab = core.getUIManager().createTab(name);
        	
    		try {
    			fxmlLoader.setController(this);
    			fxmlLoader.setLocation(getClass().getResource("/fxml/moduleImagerie.fxml"));
    			Parent root = fxmlLoader.load();	
    			tab.setContent(root);
    			
    			/*Observable<Drone>
    			
    			for (Drone drone : core.getDroneList()) {
    				droneList.add(drone.getIp());
    			}*
    			
    			this.droneSelect.setItems(core.getDroneList());
    			this.droneSelect.getSelectionModel().selectedItemProperty().addListener(
    					(ObservableValue<? extends Drone> observable, Drone oldValue, Drone newValue) -> {
    						droneLabel.setText(newValue.getIp());
    						core.getDroneList().stream().filter((dr) -> (dr.getIp().equals(newValue.getIp()))).forEach((dr) -> {
    		                     this.client = dr.getConnection();
    		               });
    					});

    			this.labelColumn.setCellValueFactory(cellData -> cellData.getValue().labelProperty());
    			this.selectColumn.setCellValueFactory(new PropertyValueFactory<>("select"));

    			this.availableLabels.add(new LabelSelect("hard hat"));
    			this.availableLabels.add(new LabelSelect("gloves"));
    			this.availableLabels.add(new LabelSelect("vest"));
    			
    			this.labelTable.setItems(this.availableLabels);

    			this.frameListView.setCellFactory(cell -> new ListCell<Image>() {
    				private final ImageView imageView = new ImageView();      
    				@Override
    				public void updateItem(Image img, boolean empty) {
    					super.updateItem(img, empty);
    					imageView.setImage(img);
    					imageView.preserveRatioProperty().setValue(true);
    					imageView.setFitHeight(150);
    					setGraphic(imageView);
    					setText(imageView.getId());
    				}
    			});      
    			this.frameListView.setOrientation(Orientation.HORIZONTAL);

    			this.frameListView.setItems(this.frameList);

    			this.currentFrame.fitWidthProperty().bind(videoPane.widthProperty());
    			this.currentFrame.fitHeightProperty().bind(videoPane.heightProperty());

    			opened = true;
    			opening = false;
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	});
    }

    private void log(String msg) {
        System.out.println("Module Imagerie : " + msg);
    }
       
    /***
     * transforme en image utilisable par ui le parametre
     * @param frame
     * @return
     *
    public Image mat2Image(Mat frame) {
        // create a temporary buffer
        MatOfByte buffer = new MatOfByte();
        Imgcodecs.imencode(".png", frame, buffer);
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }
   
    /**
     * stop le module de recherche de label et previens le drone
     *
    @FXML
    private void stopImagery(){
        if (client == null || !imageryStarted) {
            	log("Imagery already stopped or client is null");
                return;
        }
        String label = ImageryCommands.STOP_IMAGERY.label;

        try {
            client.send(new ControlImageryPacket(false));
            imageryStarted = false;
            log(label + " : Stopping imagery");
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            log("Unable to send '" + label + "' command");
        }
    }
     
    /**
     * debute le system d'imagerie du drone
     *
    @FXML
    private void startImagery(){
        if (client == null || imageryStarted) {
        	log("Imagery already started or client is null");
            return;
        }
        String label = ImageryCommands.START_IMAGERY.label;
        
        try {
            client.send(new ControlImageryPacket(true));
            imageryStarted = true;
            log(label + " : Starting imagery");
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            log("Unable to send '" + label + "' command");
        }
    }
    
    /**
     * termine le live video
     *
    @FXML
    private void stopVideo(){
        String label = ImageryCommands.STOP_LIVE.label;
        if (client == null) {
                log(label + " : Need to select a drone");
                return;
        }    
        if (!viewLive) {
                log(label + " : video already stopped");
                return;
        }
        try {
            client.send(new ViewLiveControlPacket(false));
            viewLive = false;
            log("Stopping live");
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            log("Unable to send '" + label + "' command");
        }
    }
    
    /**
     * transmet au drone la commande d'ouverture du live video
     *
    @FXML
    private void startVideo(){
    	if (client == null || viewLive) {
    		log("streaming already start or client is not set");
            return;
    	}  
        
        try {
        	this.currentFrame.setImage(new Image(getClass().getResourceAsStream("/img/loading.gif")));
            client.send(new ViewLiveControlPacket(true));
            viewLive = true;
            log(ImageryCommands.START_LIVE.label + " : Starting live");
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
            log("Unable to send '" + ImageryCommands.START_LIVE.label + "' command");
        }
    }
    
    
    /**
     * envoie les labels selectionner par l'utilisateur au drone
     *
    @FXML
    private void sendLabels() {        
        String cmd = ImageryCommands.SEND_LABELS.label;
    
        ArrayList<String> toSend = new ArrayList<>();
        this.availableLabels.stream().filter((label) -> (label.getSelect().isSelected())).forEachOrdered((label) -> {
            toSend.add(label.getLabel());
        });
        System.out.println(toSend.toString());
        if (client == null) {
            log(cmd + " : Need to select a drone to send labels");
            return;
        }
        LabelSelectionPacket packet = new LabelSelectionPacket(toSend);  
        try {
            client.send(packet);
        } catch (IOException ex) {
            log("Unable to send '" + cmd + "' command");
            ex.printStackTrace(System.err);            
        }      
    }
    */
}
