package fr.dolos.module.imagerie;

import java.io.IOException;

import fr.dolos.module.imagerie.controllers.TabController;
import fr.dolos.module.imagerie.network.packets.ControlImageryPacket;
import fr.dolos.module.imagerie.network.packets.ImageryInfosPacket;
import fr.dolos.module.imagerie.network.packets.LabeledFramePacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveControlPacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveServerInfoPacket;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.modules.UserModule;
import fr.dolos.sdk.network.NetworkManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

/**
 * Imagerie main
 * @author Mathias Hyrondelle
 */
public class MainModule extends UserModule {
	
	private static final String MODULE_NAME = "Imagerie";
	
	private TabController imagerieTab;

	@Override
	public boolean load(Core core) {
		NetworkManager networkManager = core.getNetworkManager();
		networkManager.registerDeserializer("Imagery_Infos_Packet", new ImageryInfosPacket());
        networkManager.registerDeserializer("View_Live_Server_Info_Packet", new ViewLiveServerInfoPacket());
        networkManager.registerDeserializer("Labeled_Frame_Packet", new LabeledFramePacket());
        networkManager.registerDeserializer("View_Live_Control_Packet", new ViewLiveControlPacket());
        networkManager.registerDeserializer("Control_Imagery_Packet", new ControlImageryPacket());
		
		this.imagerieTab = new TabController();
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/moduleImagerie.fxml"));
		try {
			loader.setController(imagerieTab);
			VBox pane = loader.load();
			this.imagerieTab.init();
			Tab t = core.getUIManager().createTab(MODULE_NAME);
			t.setContent(pane);
			core.getUIManager().bindWidthAndEight(pane);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void unload() {
		
	}

	@Override
	public String getName() {
		return MODULE_NAME;
	}

}
