package fr.dolos.module.imagerie.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import fr.dolos.module.imagerie.models.LabelSelect;
import fr.dolos.module.imagerie.network.StreamingProvider;
import fr.dolos.module.imagerie.network.StreamingReceiver;
import fr.dolos.module.imagerie.network.UDPStreamingProvider;
import fr.dolos.module.imagerie.network.packets.ControlImageryPacket;
import fr.dolos.module.imagerie.network.packets.ImageryInfosPacket;
import fr.dolos.module.imagerie.network.packets.LabeledFramePacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveControlPacket;
import fr.dolos.module.imagerie.network.packets.ViewLiveServerInfoPacket;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.PacketHandler;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * Handle main imagerie tab
 * @author Mathias Hyrondelle
 */
public class TabController implements StreamingReceiver {

	private static final String VIDEO_STARTED = "vid_started", IMAGE_STARTED = "img_started",
			STREAMING_PROVIDER = "streaming_provider", LABEL_BUFFER = "label_buffer";
	private static final int VIDEO_FPS = 40;

	public static Image mat2Image(Mat frame) {
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		Imgcodecs.imencode(".png", frame, buffer);
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}

	@FXML
	public VBox anchor_pane, video_pane;
	@FXML
	public SplitPane main_split;
	@FXML
	public ImageView current_frame;
	@FXML
	public ListView<Image> frame_list;
	@FXML
	public ChoiceBox<Drone> drone_select;
	@FXML
	public TableColumn<LabelSelect, String> label_column;
	@FXML
	public TableColumn<LabelSelect, CheckBox> label_selected_column;
	@FXML
	public TableView<LabelSelect> label_table;
	@FXML
	public Button start_img_button, stop_img_button, start_video_button, stop_video_button;

	private final Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	private ObservableList<LabelSelect> availableLabels;
	private Queue<Image> imageBuffer;

	/**
	 * Start VIDEO button <br/>
	 * Initialize UDP listening if not initialized<br/>
	 * Send video streaming start packet to drone <br/>
	 */
	@FXML
	public void startVideoStreaming()
	{
		final Drone d = drone_select.getValue();
		if (d == null)
		{
			logger.info("No drone selected when trying to start video streaming");
			return;
		}
		boolean started = (d.getData().get(VIDEO_STARTED) == null) ? false : ((boolean) d.getData().get(VIDEO_STARTED));
		if (started)
			return;
		d.getData().put(VIDEO_STARTED, true);
		this.current_frame.setImage(new Image(getClass().getResourceAsStream("/img/loading.gif")));
		Core.getInstance().getScheduler().runAsync(() -> {
			try {
				d.getConnection().send(new ViewLiveControlPacket(true));
			} catch (IOException | NullPointerException e)
			{
				d.getData().put(VIDEO_STARTED, false);
				this.current_frame.setImage(null);
				e.printStackTrace();
			}
			return null;
		});
	}

	/**
	 * Stop VIDEO button <br/>
	 * Send video streaming stop packet to drone
	 */
	@FXML
	public void stopVideoStreaming()
	{
		final Drone d = drone_select.getValue();
		if (d == null)
		{
			logger.info("No drone selected when trying to stop video streaming");
			return;
		}
		if (!((boolean)d.getData().get(VIDEO_STARTED)))
			return;
		StreamingProvider provider = (d.getData().get(STREAMING_PROVIDER) == null) ? null : (StreamingProvider) d.getData().get(STREAMING_PROVIDER);
		if (provider != null)
			provider.stop();
		d.getData().put(VIDEO_STARTED, false);
		Core.getInstance().getScheduler().runAsync(() -> {
			try {
				d.getConnection().send(new ViewLiveControlPacket(false));
			} catch (IOException e)
			{
				e.printStackTrace();
				d.getData().put(VIDEO_STARTED, true);
			}
			return null;
		});
	}

	/**
	 * Start IMG button <br/>
	 * Send starting packet to the currently selected drone
	 */
	@FXML
	public void startImageRecognition()
	{
		final Drone d = drone_select.getValue();
		if (d == null)
		{
			logger.info("No drone selected when trying to start image recognition");
			return;
		}
		start_img_button.setDisable(true);
		d.getData().put(IMAGE_STARTED, true);
		Core.getInstance().getScheduler().runAsync(() -> {
			try {
				d.getConnection().send(new ControlImageryPacket(true));
				if (drone_select.getValue() == d)
					stop_img_button.setDisable(false);
			} catch (IOException | NullPointerException e)
			{
				e.printStackTrace();
				if (drone_select.getValue() == d)
				{
					start_img_button.setDisable(false);
					d.getData().put(IMAGE_STARTED, false);
				}
			}
			return null;
		});
	}

	@FXML
	public void stopImageRecognition()
	{
		final Drone d = drone_select.getValue();
		if (d == null)
		{
			logger.info("No drone selected when trying to start image recognition");
			return;
		}
		stop_img_button.setDisable(true);
		d.getData().put(IMAGE_STARTED, false);
		Core.getInstance().getScheduler().runAsync(() -> {
			try {
				d.getConnection().send(new ControlImageryPacket(false));
				if (drone_select.getValue() == d)
					start_img_button.setDisable(false);
			} catch (IOException e)
			{
				e.printStackTrace();
				if (drone_select.getValue() == d)
				{
					stop_img_button.setDisable(false);
					d.getData().put(IMAGE_STARTED, true);
				}
			}
			return null;
		});
	}

	/**
	 * Init the controller, called from MainModule
	 */
	public void init()
	{
		this.imageBuffer = new ConcurrentLinkedQueue<>();
		// drone_select setup
		this.drone_select.setConverter(new StringConverter<Drone>() {

			@Override
			public String toString(Drone drone) {
				return drone.getIp();
			}

			@Override
			public Drone fromString(String string) {
				return Core.getInstance().getDroneList().stream().filter(item -> item.getIp().equals(string)).findFirst().orElse(null);
			}
		});
		this.drone_select.selectionModelProperty().addListener(new ChangeListener<SingleSelectionModel<Drone>>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends SingleSelectionModel<Drone>> observable,
					SingleSelectionModel<Drone> oldValue, SingleSelectionModel<Drone> newValue) {
				if (oldValue != null)
				{
					Drone old = oldValue.getSelectedItem();
					StreamingProvider provider = (old.getData().get(STREAMING_PROVIDER) == null) ? null : (StreamingProvider) old.getData().get(STREAMING_PROVIDER);
					if (provider != null)
						provider.stop();
					try {
						old.getConnection().send(new ViewLiveControlPacket(false));
					} catch (IOException e) {
						e.printStackTrace();
					}
					old.getData().put(VIDEO_STARTED, false);
				}

				if (newValue != null) 
				{
					Drone d = newValue.getSelectedItem();
					ConcurrentHashMap<String, Object> data = d.getData();
					ObservableList<Image> history = (data.get(LABEL_BUFFER) == null) ? null : (ObservableList<Image>) data.get(LABEL_BUFFER);
					if (history == null)
					{
						history = FXCollections.observableArrayList();
						d.getData().put(LABEL_BUFFER, history);
					}
					Bindings.bindContent(frame_list.getItems(), history);
					boolean started = (data.get(IMAGE_STARTED) == null) ? false : (boolean) data.get(IMAGE_STARTED);
					start_img_button.setDisable(started);
					stop_img_button.setDisable(!started);
					start_video_button.setDisable(false);
					stop_video_button.setDisable(true);
				}
			}
		});
		Bindings.bindContent(drone_select.getItems(), Core.getInstance().getDroneList());

		// Label selection setup
		this.availableLabels = FXCollections.observableArrayList(new LabelSelect("hard hat"), new LabelSelect("gloves"), new LabelSelect("vest"));
		this.label_column.setCellValueFactory(label -> label.getValue().labelProperty());
		this.label_selected_column.setCellValueFactory(new PropertyValueFactory<>("select"));
		Bindings.bindContent(label_table.getItems(), availableLabels);

		// Frame listing
		this.frame_list.setCellFactory(cell -> new ListCell<Image>() {
			private final ImageView imageView = new ImageView();
			@Override
			public void updateItem(Image img, boolean empty) {
				super.updateItem(img, empty);
				if (img == null || empty)
				{
					super.setGraphic(null);
					super.setText(null);
					return;
				}
				this.imageView.setImage(img);
				this.imageView.preserveRatioProperty().setValue(true);
				super.setGraphic(imageView);
				super.setText(imageView.getId());
			}
		});
		this.frame_list.setOrientation(Orientation.HORIZONTAL);

		// Frame display
		this.current_frame.fitWidthProperty().bind(video_pane.widthProperty());
		this.current_frame.fitHeightProperty().bind(video_pane.heightProperty());

		Core.getInstance().getNetworkManager().registerReceiver(this);

		Core.getInstance().getScheduler().runTimed(() -> {
			if (imageBuffer.isEmpty())
				return;
			final Image img = imageBuffer.poll();
			CompletableFuture<Void> f = new CompletableFuture<>();
			Platform.runLater(() -> {
				current_frame.setImage(img);
				f.complete(null);
			});
			try {
				f.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}, 5000, 40);
	}

	@Override
	public void onFrameReceived(Image image) {
		imageBuffer.add(image);
	}

	@Override
	public boolean accept() {
		while (imageBuffer.size() > (VIDEO_FPS * 5))
		{
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	/**
	 * Called when drone start video streaming
	 * @param serverInfoPacket
	 * @param client
	 */
	@PacketHandler
	public void onPacketReceived(ViewLiveServerInfoPacket serverInfoPacket, NetworkClient client) {
		Drone drone = drone_select.getSelectionModel().getSelectedItem();
		if (!drone.getConnection().getIp().equals(client.getIp()))
			return;
		int port = serverInfoPacket.getPort();
		StreamingProvider provider = new UDPStreamingProvider(port, drone);
		drone.getData().put(STREAMING_PROVIDER, provider);
		provider.start(this);
	}

	/**
	 * Receive available label for the drone
	 * @param infoPacket
	 * @param client
	 */
	@PacketHandler
	public void onPacketReceived(ImageryInfosPacket infoPacket, NetworkClient client) {
		this.availableLabels.clear();
		infoPacket.getLabels().forEach((label) -> {
			this.availableLabels.add(new LabelSelect(label));
		});
	}

	/*@PacketHandler
	public void onPacketReceived(LabeledFramePacket framePacket, NetworkClient client) {
		Mat frame = framePacket.getFrame();
		UUID uid = UUID.randomUUID();
		Imgcodecs.imwrite("data_received/"+ uid.toString() + ".jpg", frame);

		Image img  = mat2Image(frame);
		Drone d = Core.getInstance().getDroneList().filtered(item -> item.getConnection().getIp().equals(client.getIp())).get(0);
		if (d != null)
		{
			@SuppressWarnings("unchecked")
			ObservableList<Image> history = (d.getData().get(LABEL_BUFFER) == null) ? null : (ObservableList<Image>) d.getData().get(LABEL_BUFFER);
			if (history == null)
			{
				history = FXCollections.observableArrayList();
				d.getData().put(LABEL_BUFFER, history);
			}
			history.add(img);
		}
	}
	*/
}
