package fr.dolos.module.imagerie.network;

public interface StreamingProvider {

	public void start(StreamingReceiver receiver);
	
	public void stop();
}
