/*
 * Labeled Frame Packet class for Imagerie Module 
 */


package fr.dolos.module.imagerie.network.packets;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.dolos.sdk.network.Packet;
import fr.dolos.sdk.network.PacketDeserializer;

/**
 *
 * @author schin
 */

public class LabeledFramePacket extends JsonPacket implements PacketDeserializer{
    
    public static final String PACKET_NAME = "Labeled_Frame_Packet";
        
    private ArrayList<AnchorBox> boxes;
    
    public LabeledFramePacket() {}
    
    public LabeledFramePacket(ArrayList<AnchorBox> boxes) {
    	this.boxes = boxes;
    }
    
    public LabeledFramePacket(String data) {
    	this.boxes = new ArrayList<>();
    	
        JSONObject obj = this.deserializeData(data);

        JSONArray array = (JSONArray) obj.get("boxes");
        Iterator iter = array.iterator();

        while (iter.hasNext()) {
        	JSONObject jbox = (JSONObject) iter.next();
        	JSONArray c1 = (JSONArray) jbox.get("c1");
        	JSONArray c2 = (JSONArray) jbox.get("c2");
        	long c1x = (long) c1.get(0);
        	long c1y = (long) c1.get(1);
        	long c2x = (long) c2.get(0); 
        	long c2y = (long) c2.get(1);
        	long cls = (long) jbox.get("classe");
        	long time_stamp = (long) jbox.get("timestamp");
        	long id = (long) jbox.get("id");
            boxes.add(new AnchorBox(c1x, c1y,c2x, c2y, cls, time_stamp, id));
        }
    }
                
    @Override
    public String getName() {
        return PACKET_NAME;
    }
    
    public ArrayList<AnchorBox> getBoxes() {
    	return boxes;
    }
    
    @Override
    public void serializeData(JSONObject object) {
    	/*
    	JSONArray jboxes = new JSONArray();
    	
    	for (AnchorBox b : boxes) {
            JSONObject box = new JSONObject();
            
            box.put("classe", b.cls);
            box.put("c1x", b.c1x);
            box.put("c1y", b.c1y);
            box.put("c2x", b.c2x);
            box.put("c2y", b.c2y);
            box.put("timestamp", b.time_stamp);
            box.put("id", b.id);
            
            jboxes.add(box);
    	}
    	
    	object.put("boxes", jboxes);*/
    }
    
    @Override
    public Packet deserialize(String packet, String data){
        
        if (packet.equals(PACKET_NAME)) {
           Packet newPacket = new LabeledFramePacket(data);
           return newPacket;
       } else {
           System.out.println("Wrong type of packet : expected " + PACKET_NAME + " and got " + packet);
           return null;
       }
    }
}