package fr.dolos.module.imagerie.network.packets;

public class AnchorBox {
    public long c1x, c1y;
    public long c2x, c2y;
    public long cls;
    public long time_stamp;
    public long id;
    
    public AnchorBox(long c1x, long c1y, long c2x, long c2y, long cls, long time_stamp, long id) {
        this.c1x = c1x;
        this.c1y = c1y;
        this.c2x = c2x;
        this.c2y = c2y;
        this.cls = cls;
        this.time_stamp = time_stamp;
        this.id = id;
    }
}