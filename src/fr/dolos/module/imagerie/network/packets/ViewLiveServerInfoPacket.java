/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.dolos.module.imagerie.network.packets;

import fr.dolos.sdk.network.Packet;
import fr.dolos.sdk.network.PacketDeserializer;
import org.json.simple.JSONObject;

/**
 *
 * @author marie
 */
public class ViewLiveServerInfoPacket extends JsonPacket implements PacketDeserializer{

    public static final String PACKET_NAME = "View_Live_Server_Info_Packet";
    
    private int port;
    
    public ViewLiveServerInfoPacket() {}
    
    public ViewLiveServerInfoPacket(int port) {
        this.port = port;
    }
    
    public ViewLiveServerInfoPacket(String data) {
        JSONObject obj = this.deserializeData(data);
        this.port = Integer.valueOf(Long.toString((long)obj.get("port")));
    }
    
    public int getPort() {
        return port;
    }
    
    @Override
    public void serializeData(JSONObject object) {
       object.put("port", this.port);
    }

    @Override
    public String getName() {
        return PACKET_NAME;
    }

    @Override
    public Packet deserialize(String packet, String data) {
       if (packet.equals(PACKET_NAME)) {
           Packet newPacket = new ViewLiveServerInfoPacket(data);
           return newPacket;
       } else {
           System.out.println("Wrong type of packet : expected " + PACKET_NAME + " and got " + packet);
           return null;
       }
    }
    
}
