package fr.dolos.module.imagerie.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import fr.dolos.module.imagerie.controllers.TabController;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.models.Drone;

public class UDPStreamingProvider implements StreamingProvider, Callable<Void> {

	private final int port;
	private DatagramSocket socket;
	private StreamingReceiver receiver;
	private volatile boolean loop = false;
	private final Drone drone;
	private final Logger logger;
	private StringBuilder buffer;

	public UDPStreamingProvider(int port, Drone drone) {
		this.port = port;
		this.drone = drone;
		this.logger = Logger.getLogger(getClass().getCanonicalName());
	}

	@Override
	public void start(StreamingReceiver receiver) {
		this.receiver = receiver;
		loop = true;
		Core.getInstance().getScheduler().runAsync(this);
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	@Override
	public Void call() throws Exception {
		try {
			String text = "ClientlLiveUDP";
			logger.info("Opening server on UDP port: " + port);
			DatagramPacket data = new DatagramPacket(text.getBytes(), text.length(), InetAddress.getByName(drone.getIp()), port);
			socket = new DatagramSocket();
			socket.send(data);
			buffer = new StringBuilder();
			logger.info("Looping");
			while (loop && receiver.accept()) {
				logger.info("Reading socket");
				Mat frameReceived = readSocket();
				logger.info("Read socket");
				if (frameReceived == null) {
					continue;
				}
				logger.info("Queuing frame");
				receiver.onFrameReceived(TabController.mat2Image(frameReceived));
			}
		} catch (IOException ex) {
			ex.printStackTrace();   
		}
		return null;
	}

	private Mat readSocket() {
		String packet_begin_str = "<Dolos_STR>";
		String packet_end_str = "</Dolos_STR>";
		byte[] buff = new byte[4096];
		boolean run = true;

		logger.info("Looping");
		while (run) {
			try {
				DatagramPacket dataReceived = new DatagramPacket(buff, 4096);
				socket.receive(dataReceived);
				buffer.append(new String(dataReceived.getData(), 0, dataReceived.getLength()));
				if (buffer.toString().contains(packet_begin_str) && buffer.toString().contains(packet_end_str))
				{
					logger.info("Found packet_end_str");
					run = false;
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				return null;
			}

		}
		// Get data between NETWORK_MSG_START and NETWORK_MSG_END including NETWORK_MSG_START/END
		String raw = buffer.substring(buffer.indexOf(packet_begin_str), buffer.indexOf(packet_end_str) + packet_end_str.length());
		buffer.delete(buffer.indexOf(raw), raw.length()); // Delete data from builder
		String rawFiltered = raw.substring(packet_begin_str.length(), raw.indexOf(packet_end_str));
		
		byte[] packet;
        packet = Arrays.copyOfRange(rawFiltered.getBytes(), 0, rawFiltered.length());

		logger.info("now ret");
		Mat encoded = new Mat(1, packet.length, CvType.CV_8U);
		encoded.put(0, 0, packet);
		return Imgcodecs.imdecode(encoded, Imgcodecs.IMREAD_COLOR);
	}
}
