package fr.dolos.module.imagerie.network;

import java.io.File;
import java.util.logging.Logger;

import fr.dolos.sdk.Core;
import javafx.scene.image.Image;

public class LocalStreamingProvider implements StreamingProvider {
	
	private static final String IMG_FOLDER = "/home/mathias/Téléchargements/video";

	private final Logger logger = Logger.getLogger(this.getClass().getCanonicalName());
	private File imgFolder;
	private volatile boolean run = false;
	
	@Override
	public void start(final StreamingReceiver receiver) {
		imgFolder = new File(IMG_FOLDER);
		if (!imgFolder.exists())
		{
			logger.warning("Folder " + IMG_FOLDER + " does not exists");
			return;
		}
		run = true;
		Core.getInstance().getScheduler().runOnDedicated(() -> {
			File[] files = imgFolder.listFiles((dir, filename) -> filename.endsWith(".jpg"));
			for (File f : files)
			{
				if (!run) return null;
				if (!receiver.accept()) return null;
				Image img = new Image(f.toURI().toString());
				receiver.onFrameReceived(img);
			}
			return null;
		});
	}

	@Override
	public void stop() {
		run = false;
	}
	
	

}
