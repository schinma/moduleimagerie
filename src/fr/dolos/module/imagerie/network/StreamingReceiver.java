package fr.dolos.module.imagerie.network;

import javafx.scene.image.Image;

public interface StreamingReceiver {

	public void onFrameReceived(Image image);
	public boolean accept();
}
