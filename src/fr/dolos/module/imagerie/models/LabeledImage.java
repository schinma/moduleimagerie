/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.dolos.module.imagerie.models;

import javafx.scene.image.Image;

/**
 *
 * @author schin
 */
public class LabeledImage {

    private String label;
    private Image image;

    LabeledImage(Image img, String label) {
        this.image = img;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
